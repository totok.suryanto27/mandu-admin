@extends('layouts.layout')

@section('content')

    {!!
        Form::open([
            'action' => [
                'HomeController@storeAttraction'
            ],
            'method' => 'POST',
            'class' => 'form-horizontal form-label-left input_mask',
            'id' => 'update-on-the-fly-form',
            'files' => 'true',
        ])
    !!}

    <div id="page-wrapper">
        <div class="row">

            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
        <!-- /.row -->

        <div class="col-lg-12">
            <h2>CREATE NEW ATTRACTION</h2>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Attraction Name</h4>
                                        <input type="text"
                                        	class="form-control"
                                        	name="attraction-name"
                                        	id=""
                                        	placeholder="Attraction name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>City ID</h4>
                                        <select id="city"
                                            name="city"
                                            class="form-control"
                                            value="">
                                            <option>Select Your City</option>

                                            @foreach($city as $key => $item)
                                                @if ($item->parent_id != '1' && $item->parent_id != '0')
                                                    <option value="{{ $item->id }}">{{ $item->place_name }}</option>

                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Opening Hours</h4>
                                        <input type="time"
                                        	class="form-control"
                                        	name="opening-hours"
                                        	id=""
                                        	placeholder="Opening Hours">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Closing Hours</h4>
                                        <input type="time"
                                        	class="form-control"
                                        	name="closing-hours"
                                        	id=""
                                        	placeholder="Closing Hours">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Theme</h4>

                                            @foreach($type as $key => $item)
                                                <input id="theme" name="theme[]" type="checkbox" value="{{ $item->id }}">
                                                <label for="theme">{{ $item->attraction_type_name }}</label>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Difficulties Level</h4>
                                        <select id="level"
                                            name="level"
                                            class="form-control"
                                            value="">
                                            <option>Select Level</option>

                                            @foreach($level as $key => $item)
                                                    <option value="{{ $item->id }}">{{ $item->diff_level }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Capacity</h4>
                                        <select id="capacity"
                                            name="capacity"
                                            class="form-control"
                                            value="">
                                            <option>Capacity</option>

                                            @foreach($capacity as $key => $item)
                                                    <option value="{{ $item->id }}">{{ $item->capacityseat }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <input id="parking" name="parking" type="checkbox" value="1">
                                        <label for="parking">Parking</label><br>
                                        <input id="ac" name="ac" type="checkbox" value="1">
                                        <label for="ac">AC</label><br>
                                        <input id="atm" name="atm" type="checkbox" value="1">
                                        <label for="atm">ATM</label><br>
                                        <input id="giftshop" name="giftshop" type="checkbox" value="1">
                                        <label for="giftshop">Giftshop</label><br>
                                        <input id="toilet" name="toilet" type="checkbox" value="1">
                                        <label for="toilet">Toilet</label><br>
                                        <input id="resto" name="resto" type="checkbox" value="1">
                                        <label for="resto">Restaurant</label><br>
                                        <input id="mushola" name="mushola" type="checkbox" value="1">
                                        <label for="mushola">Mushola</label><br>
                                        <input id="wifi" name="wifi" type="checkbox" value="1">
                                        <label for="wifi">Wifi</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Location</h4>
                                        <input type="text"
                                        	class="form-control"
                                        	name="searchmap"
                                        	id="searchmap">
                                        <div class="map-canvas" id="map-canvas"></div>
                                        {!! Mapper::render() !!}

                                        <h4>Latitude</h4>
                                        <input type="text"
                                        	class="form-control"
                                        	name="lat"
                                        	id="lat">

                                        <h4>Longitude</h4>
                                        <input type="text"
                                        	class="form-control"
                                        	name="lng"
                                        	id="lng">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <h4>Upload Image</h4>
                                        <input type="file" name="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <button class="btn btn-default btn-sm pull-right button-att" type="submit">CREATE</button>
            </div>
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4">

            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
        <footer>
        <div>
            <p>Copyright : PT. Mandu Wisata Indonesia (2017)</p>
        </div>
    </footer>
    </div>
    <!-- /#page-wrapper -->

</div>

{!! Form::close() !!}

@endsection
