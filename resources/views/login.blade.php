<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin Mandu</title>

        <link rel="stylesheet" type="text/css" href="m-wadmin-style.css">

        <style>
            * {
            	box-sizing: border-box;
            }
            .m-wadmin-layout {
            	width: 100%;
            	margin: 0;
            	padding: 0;
            	font-family: "lato", normal;
            }
            .s-50 {
            	width: 50%;
            	display: block;
            	position: relative;
            	float: left;
            }

            .img-responsive {
                width: 100%;
                height: auto;
                display: block;
            }
            .m-content {
            	padding: 10px 35px;
            	background: #f9f9f9;
            	display: block;
            }

            .m-center-content {
            	text-align: center;
            	margin: 0 auto;
            	display: block;
            	padding: 25px;
            }

            .logo {
            	width: 100px;
            	height: auto;
            	margin: 20px auto;
            }

            .welcome-line {
            	font-size: 20px;
            	font-weight: 600;
            	text-align: center;
            	padding-top: 20px;
            }

            .bottom-line {
            	font-size: 14px;
            	font-weight: 200;
            	text-align: center;
            	padding-top: 50px;
            }

            .headline {
            	font-size: 18px;
            	font-weight: 500;
            	text-align: center;
            	margin: 10px;
            	padding: 10px;
            }

            .form-layout {
            	display: block;
            }
            .form-line {

                padding: 8px;
                color: #808285;

            }

            .form-line label {
            	display: -webkit-box;
            	padding: 10px;
            	text-transform: uppercase;
            }

            .input-costum {

                padding: 8px;
                width: 96%;
               	height: 45px;
                border: 1px solid #d1d3d4;
                border-radius: 3px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
            }

            .button {
                background-color: #FF6700;
                border: none;
                color: white;
                padding: 10px 16px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                position: absolute;
                right: 80px;
            }
        </style>

    </head>
    <body class="m-wadmin-layout">
        <section class="s-50">
            <img src="{{ asset ('frontImage/photobglandingpagesquare.jpg') }}" class="img-responsive"/>
        </section>

        <section class="s-50 m-content">
            <div class="m-center-content">
                <img src="{{ asset ('frontImage/mandu-logo.png') }}" class="img-responsive logo"/>
                <h3 class="welcome-line">
                Welcome Here! You May Login Below
                </h3>

                <form class="form-layout" action="/login" method="POST">
                    <div class="form-line">
                        <label>Username or Email Address</label>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input class="input-costum" type="text" name="username" placeholder="input your username" />
                    </div>

                    <div class="form-line">
                        <label>Password</label>
                        <input class="input-costum" type="password" name="password" placeholder="input your password" />
                    </div>
                    <div class="checkbox form-line">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                        </label>
                    </div>
                    <div class="form-line">
                        <button class="btn btn-default btn-sm pull-right button" type="submit">LOGIN</button>
                    </div>
                    <h5 class="bottom-line">
                    Copyright: PT.Mandu Wisata Indonesia (2017)
                    </h5>
                </form>
            </div>
        </section>
    </body>
</html>
