@extends('layouts.layout')

@section('content')
    <div id="page-wrapper">
        <div class="row">

            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
        <!-- /.row -->

        <div class="col-lg-12">
        <!-- /.row -->
        <div class="row">
            <div>
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <h2>{{ $data->att_name }}</h2>
                        <tr>
                            <th>ID</th>
                            <td>{{ $data->id }}</td>

                        </tr>
                        <tr>
                            <th>City</th>
                            <td>{{ $data->city->place_name }}</td>
                        </tr>
                        <tr>
                            <th>Capacity</th>
                            <td>{{ $data->capacity }}</td>
                        </tr>
                        <tr>
                            <th>Difficulties</th>
                            <td>{{ $data->diff_level }}</td>
                        </tr>
                        <tr>
                            <th>Open</th>
                            <td>{{ $data->openinghours }}</td>
                        </tr>
                        <tr>
                            <th>Close</th>
                            <td>{{ $data->closinghours }}</td>
                        </tr>
                    </thead>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4">

            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
        <footer>
        <div>
            <p>Copyright : PT. Mandu Wisata Indonesia (2017)</p>
        </div>
    </footer>
    </div>
    <!-- /#page-wrapper -->

</div>

@endsection
