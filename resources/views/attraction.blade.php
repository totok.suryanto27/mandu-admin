@extends('layouts.layout')

@section('content')
    <div id="page-wrapper">
        <div class="row">

            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            @foreach ($countData as $key => $countDatas)
                @foreach ($province as $keys => $provinces)
                    @if ($key == $keys && $countDatas > 60)
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-13 text-right">
                                            <center>
                                                <div>{{ $provinces->place_name }}</div>
                                                <div><font size="+3" color="#FF0000">{{ $countDatas }}</font></div>
                                            </center>
                                        </font></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                @endforeach
            @endforeach
        </div>
        <!-- /.row -->

            <div class="col-lg-12">
            <div class="row">

            <a href="#"> <h4 align="right" style="color:#FF0000"> View More >>>></h4></a>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <!-- /.row -->
        <div class="row">
            <div class="pull-right">
                <table>
                    <tr>
                        <td bgcolor="white"><font color="white"><h5>CREATE MEMBER ATTRACTIONS</h5> </font></td>
                        <button class="btn btn-default btn-sm button-att" type="submit"><a href="/newattraction">
                        CREATE MEMBER ATTRACTIONS</a></button>
                    </tr>
                </table>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                    <div>
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>

                            </thead>
                            <tbody>
                                @foreach ($data as $att)
                                    <div class="responsive">
                                        <div class="gallery">
                                                <img src="/assets/mandu-logo-icon.png" alt="Trolltunga Norway" width="300" height="200">
                                            <div class="desc">

                                                <h6 style="font-weight: bold">{{ $att->att_name }}</h6>

                                                <p>Location : {{ $att->city->place_name }}</p>
                                                <p>Open/Close : {{ $att->openinghours }}/{{ $att->closinghours }}</p>
                                                <a href="{{ URL::to('/attraction'.'/'.$att->id) }}" class="btn btn-sm button-att" type="submit">REVIEW</a>
                                                <br>
                                                <br>

                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </tbody>
                                <tr>
                                  <td colspan="2">
                                    <div class="pagination pull-right">{!! str_replace('/?', '?', $data->render()) !!}</div>
                                  </td>
                                </tr>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4">

            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
        <footer>
        <div>
            <p>Copyright : PT. Mandu Wisata Indonesia (2017)</p>
        </div>
    </footer>
    </div>
    <!-- /#page-wrapper -->

</div>

@endsection
