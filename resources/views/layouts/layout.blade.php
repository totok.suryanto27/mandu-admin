<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/css/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- DataTables CSS -->
    <link href="/css/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/css/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxvfDPIRt6A1dFkzGwq5NMX9x8JY1Pxlw&libraries=places"
    type="text/javascript"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">



            </div>

            <ul class="nav navbar-top-links navbar-right">
					<div class="row">
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
												<center>
												<div class="text-muted small">Total Downloads</div>
												<div>150,0000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
											<center>
												<div class="text-muted small">Attractions</div>
												<div>120,0000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
												<center>
												<div class="text-muted small">Registered Users</div>
												<div>900,000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
												<center>
												<div class="text-muted small">Travel Contributors</div>
												<div>850,000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
												<center>
												<div class="text-muted small">Hosts</div>
												<div>750,000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
												<center>
												<div class="text-muted small">Tour Drivers</div>
												<div>985,0000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
					<li>
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-right">
												<center>
												<div class="text-muted small">Tour Guides</div>
												<div>600,000</div>
											</center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</li>
                    <li style="margin-left:50px">
                    <a>
                        <table>
									<tr align="right">
									<td align="right">
                                    <img src="/assets/mandu-logo-icon.png" width="40" height="32" style="margin-right:5px">
									</td>

									<td>
									Admin
                                    <p class="text-muted small" align="left">Admin
                                    </p>
									</td>
									</tr>
						</table><!--<i class="fa fa-caret-down"></i>-->
                    </a>

                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
			</div>
            </ul>
            <!-- /.navbar-header -->

            {{-- <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <img src="/assets/mandu-logo-icon.png" width="40" height="32">
                    </a>
                    {{-- <p>{{ $name }}</p> --}}
                    {{-- <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="Login/login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul> --}}
                    <!-- /.dropdown-user -->
                {{-- </li> --}}
                <!-- /.dropdown -->
            {{-- </ul> --}}
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation navbar-collapse">

                <div class="sidebar-nav navbar-collapse col-xs-3">
				<center><a href="index.html"><img src="/assets/mandu-logo-icon.png" width="40" height="32"><h5></h5></a>
                    <ul class="nav" id="side-menu" style="background-color:#FF6700">
                        <li style="border:hidden">
                            <a href="{{ URL::to('/home') }}"><img src="/assets/home-icon.png"> <p style="color:#FFFFFF">Home</p></a>
                        </li>
                        <li style="border:hidden">
                            <a href="{{ URL::to('/attraction') }}"><img src="/assets/attraction-icon.png"> <p style="color:#FFFFFF">Attraction</p></a>
                        </li>
                        <li style="border:hidden">
                            <a href=""><img src="/assets/user-icon.png"> <p style="color:#FFFFFF">Users</p></a>
                        </li>
						<li style="border:hidden">
                            <a href=""><img src="/assets/host-icon.png"> <p style="color:#FFFFFF">Host</p></a>
                        </li>
						 <li style="border:hidden">
                            <a href=""><img src="/assets/tour-drivers-icon.png"> <p style="color:#FFFFFF">Tour Drivers</p></a>
                        </li>
						 <li style="border:hidden">
                            <a href=""><img src="/assets/tour-guides-icon.png"> <p style="color:#FFFFFF">Tour Guide</p></a>
                        </li>
						<li style="border:hidden">
                            <a href="{{ URL::to('/logout') }}"><img src="/assets/logout-icon.png"></a>
                        </li>
				</center>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        @yield('content')
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/css/vendor/raphael/raphael.min.js"></script>
    <script src="/css/vendor/morrisjs/morris.min.js"></script>
    <script src="/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="/css/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/css/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/css/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <script>
        var map = new google.maps.Map(document.getElementById('map-canvas'),{
            center:{
                lat: -6.914744,
                lng: 107.609810
            },
            zoom: 10
        });

        var marker = new google.maps.Marker({
            position:{
                lat: -6.914744,
                lng: 107.609810
            },
            map: map,
            draggable: true
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addListener(searchBox, 'places_changed', function(){

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0; place=places[i];i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(10);
        });

        google.maps.event.addListener(marker, 'position_changed', function(){

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        })
    </script>

</body>

</html>
