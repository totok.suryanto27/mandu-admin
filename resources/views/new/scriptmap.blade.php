<script>
$(document).ready(function(){
    var map = new google.maps.Map(document.getElementById('map-canvas'),{
        center:{
            lat: -6.914744,
            lng: 107.609810
        },
        zoom: 10
    });

    var marker = new google.maps.Marker({
        position:{
            lat: -6.914744,
            lng: 107.609810
        },
        map: map,
        draggable: true
    });

    var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

    google.maps.event.addListener(searchBox, 'places_changed', function(){

        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        var i, place;

        for(i=0; place=places[i];i++){
            bounds.extend(place.geometry.location);
            marker.setPosition(place.geometry.location);
        }

        map.fitBounds(bounds);
        map.setZoom(10);
    });

    google.maps.event.addListener(marker, 'position_changed', function(){

        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();

        $('#lat').val(lat);
        $('#lng').val(lng);
    })

});
</script>
