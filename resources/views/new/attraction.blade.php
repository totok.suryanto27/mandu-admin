@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">

			<!-- Ajax dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4>Attraction</h4>
						<a href="new_att" class="btn btn-deepOrange navbar-toolbar-right navbar-right"
            style="margin-right: 15px;" role="button">Create Member Attraction</a>

					</header><!-- .widget-header -->
					<hr class="widget-separator">
          <div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Attraction Name</th>
										<th>City</th>
										<th>Post User</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($data as $att)

									<tr>
										<td>{{ $att->att_name }}</td>
										<td>{{ $att->city->place_name }}</td>
										<td>{{ $att->post_user }}</td>
										<td><a href="{{ URL::to('/attraction'.'/'.$att->id) }}" class="btn btn-deepOrange pull-right" role="button">Review</a></td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
				</div><!-- .widget -->
			</div><!-- END column -->
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
@endsection
