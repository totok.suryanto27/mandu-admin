@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
		</div><!-- .row -->

		<div class="row">
			<div class="col-md-2 col-sm-4">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-primary"><span class="counter" data-plugin="counterUp">0</span></h3>
						</div>
					</div>
					<footer class="widget-footer bg-primary">
						<small>Downloads</small>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-2 col-sm-4">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-danger"><span class="counter" data-plugin="counterUp">{{ $userCount }}</span></h3>
						</div>
					</div>
					<footer class="widget-footer bg-danger">
						<small>Users</small>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-2 col-sm-4">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-success"><span class="counter" data-plugin="counterUp">{{ $contributeCount }}</span></h3>
						</div>
					</div>
					<footer class="widget-footer bg-success">
						<small>Contributors</small>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-2 col-sm-4">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-warning"><span class="counter" data-plugin="counterUp">{{ $homestayCount }}</span></h3>
						</div>
					</div>
					<footer class="widget-footer bg-warning">
						<small>Homestay</small>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-2 col-sm-4">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-primary"><span class="counter" data-plugin="counterUp">{{ $driverCount }}</span></h3>
						</div>
					</div>
					<footer class="widget-footer bg-primary">
						<small>Drivers</small>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-2 col-sm-4">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-danger"><span class="counter" data-plugin="counterUp">{{ $guideCount }}</span></h3>
						</div>
					</div>
					<footer class="widget-footer bg-danger">
						<small>Guides</small>
					</footer>
				</div><!-- .widget -->
			</div>
		</div><!-- .row -->
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-primary"><span class="counter" data-plugin="counterUp">0</span></h3>
							<small class="text-color">Tour Bookings</small>
						</div>
					</div>
					<footer class="widget-footer bg-primary">
						<span class="small-chart pull-right" data-plugin="sparkline" data-options="[4,3,5,2,1], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-3 col-sm-6">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-danger"><span class="counter" data-plugin="counterUp">0</span></h3>
							<small class="text-color">Homestay Bookings</small>
						</div>
					</div>
					<footer class="widget-footer bg-danger">
						<span class="small-chart pull-right" data-plugin="sparkline" data-options="[1,2,3,5,4], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-3 col-sm-6">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-success"><span class="counter" data-plugin="counterUp">0</span></h3>
							<small class="text-color">Total Transactions</small>
						</div>
					</div>
					<footer class="widget-footer bg-success">
						<span class="small-chart pull-right" data-plugin="sparkline" data-options="[2,4,3,4,3], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
					</footer>
				</div><!-- .widget -->
			</div>

			<div class="col-md-3 col-sm-6">
				<div class="widget stats-widget">
					<div class="widget-body clearfix">
						<div class="pull-left">
							<h3 class="widget-title text-warning"><span class="counter" data-plugin="counterUp">0</span></h3>
							<small class="text-color">Canceled Transactions</small>
						</div>
					</div>
					<footer class="widget-footer bg-warning">
						<span class="small-chart pull-right" data-plugin="sparkline" data-options="[5,4,3,5,2],{ type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
					</footer>
				</div><!-- .widget -->
			</div>
		</div><!-- .row -->

		<div class="row">
			<div class="col-md-12">
				<!-- .widget -->
			</div>
		</div><!-- .row -->

		<div class="row">
			<div class="col-md-8">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Attraction</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div id="map-canvas" style="width: 100%;height: 400px;">
                {!! Mapper::render() !!}
            </div>

					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

			<div class="col-md-4">
				<div class="widget countries-widget">
				  <header class="widget-header">
					<h4 class="widget-title">Top Province</h4>
				  </header><!-- .widget-header -->
				  <hr class="widget-separator">
				  <div class="widget-body">
						<div class="list-group m-0">
                            @foreach ($countData as $key => $countDatas)
                                @foreach ($province as $keys => $provinces)
                                    @if ($key == $keys && $countDatas > 60)
							<a href="#" class="list-group-item clearfix">
								<span class="pull-left fw-500 fz-md">{{ $provinces->place_name }}</span>
								<div class="pull-right"><span data-plugin="counterUp">{{ $countDatas }}</span></div>
							</a><!-- .list-group-item -->
                                    @endif
                                @endforeach
                            @endforeach
					</div><!-- .list-group -->
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div>
		</div><!-- .row -->

		<div class="row">

		</div><!-- .row -->

		<div class="row">

		</div><!-- .row -->

		<div class="row">

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .wrap -->

@include('new.scripthome')

@endsection
