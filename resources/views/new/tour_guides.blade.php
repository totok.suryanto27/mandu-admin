@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">{{ $dataCount }} Tour Guides</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                <thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Phone Number</th>
										<th>City</th>
										<th>Date of Birth</th>
										<th>Last Login</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($data as $member)
                                        @if ($member->role == 1)
                                            <tr>
                                                <td>{{ $member->name }}</td>
                                                <td>{{ $member->email }}</td>
                                                <td>{{ $member->phone }}</td>
                                                <td>{{ $member->birth_place }}</td>
                                                <td>{{ $member->birth_date }}</td>
                                                <td>{{ $member->last_login }}</td>
                                            </tr>

                                        @endif

                                    @endforeach
								</tbody>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@endsection
