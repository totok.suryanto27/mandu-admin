@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">{{ $dataCount }} Pick up Point</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Place Name</th>
										<th>City</th>
										<th>Province</th>
										<th>Country</th>
										<th>Type</th>
										<th>Location</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($data as $pickup)

									<tr>
										<td>{{ $pickup->place_name }}</td>
										<td>{{ $pickup->city_name }}</td>
										<td>{{ $pickup->province_name }}</td>
										<td>{{ $pickup->country_name }}</td>
										<td>{{ $pickup->pickup_type }}</td>
										<td>{{ $pickup->lat }} , {{ $pickup->lon }}</td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@endsection
