<script>
$(document).ready(function(){
    var map = new google.maps.Map(document.getElementById('map-canvas'),{
        center:{
            lat: {{ $data->lat }},
            lng: {{ $data->lot }}
        },
        zoom: 12
    });

    var marker = new google.maps.Marker({
        position:{
            lat: {{ $data->lat }},
            lng: {{ $data->lot }}
        },
        map: map,
        draggable: true
    });

    });

</script>
