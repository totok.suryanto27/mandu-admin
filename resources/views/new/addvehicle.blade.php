@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">ADD VEHICLE</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
            {!!
                Form::open([
                    'action' => [
                        'HomeController@storeVehicle'
                    ],
                    'method' => 'POST',
                    'files' => 'true',
                ])
            !!}
							<div class="form-group col-sm-3">
								<label for="">Mode</label>
									<select id="mode" name="mode" class="form-control" data-plugin="select2">
										<option value="Land">Land</option>
										<option value="Air">Air</option>
										<option value="Water">Water</option>
									</select>
							</div>
							<div class="form-group col-sm-3">
								<label for="">Vehicle Type</label>
									<select id="vectype" name="vectype" class="form-control" data-plugin="select2">
                    <option>Select Vehicle Type</option>
                      <option value="car">Car</option>
                      <option value="bus">Bus</option>
                      <option value="plane">Plane</option>
                      <option value="water vec">Water Vehicle</option>
									</select>
							</div>
							<div class="form-group col-sm-3">
								<label for="">Brand Name</label>
                <select id="brandname" name="brandname" class="form-control" data-plugin="select2">
                  <option>Select Brand</option>

                  @foreach($brandname as $key => $item)
                          <option value="{{ $item }}">{{ $item }}</option>

                  @endforeach
                </select>
							</div>
							<div class="form-group col-sm-3">
								<label for="">Brand Type</label>
								<input type="text" class="form-control" id="brandtype" name="brandtype" placeholder="Brand Type">
							</div>
							<div class="form-group col-sm-12">
									<div class="form-group col-sm-3">
										<label for="">Year</label>
											<select id="year" name="year" class="form-control" data-plugin="select2">
                        <option>Select Year</option>

                        @foreach($year as $key => $item)
                                <option value="{{ $item }}">{{ $item }}</option>

                        @endforeach
											</select>
									</div>
									<div class="form-group col-sm-3">
										<label for="exampleInputPassword1">Capacity</label>
											<select id="capacity" name="capacity" class="form-control" data-plugin="select2">
                        <option>Select Vehicle Capacity</option>

                        @foreach($capacity as $key => $item)
                                <option value="{{ $item }}">{{ $item }}</option>

                        @endforeach
											</select>
									</div>
							</div>
						<div class="widget">
							<header class="widget-header">
							</header><!-- .widget-header -->
							<hr class="widget-separator">
						</div><!-- .widget -->
						<button type="submit" class="btn btn-primary btn-md">Create</button>
            {!! Form::close() !!}
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@endsection
