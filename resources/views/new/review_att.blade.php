@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">

			<!-- Ajax dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4>{{ $data->att_name }}</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<table id="responsive-datatable" data-plugin="DataTable" data-options="{
									ajax: '../api/json/dataTable.json',
									responsive: true,
									keys: true
								}" class="table table-striped" cellspacing="0" width="100%">
							<thead>
							</thead>
							<tbody>
                                <tr>
                                    <td>City</td>
                                    <td>{{ $data->city->place_name }}</td>
                                </tr>
                                <tr>
                                    <td>Open</td>
                                    <td>{{ $data->openinghours }}</td>
                                </tr>
                                <tr>
                                    <td>Close</td>
                                    <td>{{ $data->closinghours }}</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>
                                    @foreach ($theme as $value)
                                        {{ $value->attraction_type_name }} <br>
                                    @endforeach</td>
                                </tr>
                                <tr>
                                    <td>Level</td>
                                    <td>{{ $level->diff_level }}</td>
                                </tr>
                                <tr>
                                    <td>Capacity</td>
                                    <td>{{ $capacity->capacityseat }}</td>
                                </tr>
                                <tr>
                                    <td>Location</td>
                                    <td>

        										<div id="map-canvas" style="width: 50%;height: 300px;">
                                {!! Mapper::render() !!}
                            </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Images</td>
                                    <td>
                                        @foreach ($image as $img)

                                            <div class="col-md-3 col-sm-4">
                                                <div class="thumbnail white pull-right">
                                                    <div class="single-product">
                                                        <figure>

                                                            <img src="{{ $path.'/'.$img->pathfile.$img->filename }}" alt="" width="300" height="200">

                                                        </figure>
                                                    </div> <!-- end .single-product -->
                                                </div>
                                            </div><!-- END column -->

                                    @endforeach
                                    </td>
                                </tr>

							</tbody>
							<tfoot>
                                <tr>
                                    <td></td>
                                    <td>
                                      <a href="{{ URL::to('/delete_att'.'/'.$data->id) }}"
                                        class="btn btn-danger navbar-toolbar-right navbar-right"
                                        style="margin-right: 5px;" role="button">Delete</a>

                                      <a href="{{ URL::to('/edit_att'.'/'.$data->id) }}"
                                        class="btn btn-deepOrange navbar-toolbar-right navbar-right"
                                        style="margin-right: 10px;" role="button">Edit</a>
                                    </td>
                                </tr>
							</tfoot>
						</table>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@include('new.scriptreview')

@endsection
