@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">{{ $dataCount }} Vehicle</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Mode</th>
										<th>Vehicle Type</th>
										<th>Brand Name</th>
										<th>Brand Type</th>
										<th>Capacity</th>
										<th>Year</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($data as $vehicle)

									<tr>
										<td>{{ $vehicle->moda }}</td>
										<td>{{ $vehicle->vectype }}</td>
										<td>{{ $vehicle->brandname }}</td>
										<td>{{ $vehicle->brandtype }}</td>
										<td>{{ $vehicle->capacity }}</td>
										<td>{{ $vehicle->yearprod }}</td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@endsection
