<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	<link rel="shortcut icon" sizes="196x196" href="../new/assets/images/mandu-logo-196x196.png">
	<title>Mandu Administrator</title>

	<link rel="stylesheet" href="../new/libs/bower/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../new/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css">
	<!-- build:css ../assets/css/app.min.css -->
	<link rel="stylesheet" href="../new/libs/bower/animate.css/animate.min.css">
	<link rel="stylesheet" href="../new/libs/bower/fullcalendar/dist/fullcalendar.min.css">
	<link rel="stylesheet" href="../new/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css">
	<link rel="stylesheet" href="../new/assets/css/bootstrap.css">
	<link rel="stylesheet" href="../new/assets/css/core.css">
	<link rel="stylesheet" href="../new/assets/css/app.css">
	<!-- endbuild -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
	<script src="../new/libs/bower/breakpoints.js/dist/breakpoints.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="http://maps.google.com/maps/api/js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>


	<script>
		Breakpoints();
	</script>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-deepOrange">
<!--============= start main area -->

<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top deepOrange">

  <!-- navbar header -->
  <div class="navbar-header">
    <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-box"><span class="hamburger-inner"></span></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-more"></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-search"></span>
    </button>

    <a href="/home" class="navbar-brand">
      <span class="brand-icon"><i class=""></i></span>
      <span class="brand-name">Mandu</span>
    </a>
  </div><!-- .navbar-header -->

  <div class="navbar-container container-fluid">
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">
        <li class="hidden-float hidden-menubar-top">
          <a href="javascript:void(0)" role="button" id="menubar-fold-btn" class="hamburger hamburger--arrowalt is-active js-hamburger">
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
          </a>
        </li>
        <li>
          <h5 class="page-title hidden-menubar-top hidden-float">Admin Mandu</h5>
        </li>
      </ul>
    </div>
  </div><!-- navbar-container -->
</nav>
<!--========== END app navbar -->

<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md">
          <a href="javascript:void(0)"><img class="img-responsive" src="../new/assets/images/mandu-logo-icon.png" alt="avatar"/></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username">Admin Mandu</a></h5>
          <ul>
                <small>Administrator</small>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
        <li class="has-submenu">
          <a href="{{asset('/home')}}">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">Home</span>
          </a>
					<ul class="submenu">
          </ul>
        </li>

        <li class="has-submenu">
          <a href="{{asset('/attraction')}}">
            <i class="menu-icon zmdi zmdi-layers zmdi-hc-lg"></i>
            <span class="menu-text">Attractions</span>
            <span class="label label-warning menu-label"></span>
          </a>
					<ul class="submenu">
          </ul>
        </li>

        <li class="has-submenu">
          <a href="{{asset('/users')}}">
            <i class="menu-icon fa fa-user zmdi-hc-lg"></i>
            <span class="menu-text">Users</span>
          </a>
          <ul class="submenu">
          </ul>
        </li>

        <li class="has-submenu">
          <a href="{{asset('/host')}}">
            <i class="menu-icon zmdi zmdi-inbox zmdi-hc-lg"></i>
            <span class="menu-text">Host</span>
          </a>
          <ul class="submenu">
          </ul>
        </li>

        <li>
          <a href="{{asset('/tourdriver')}}">
            <i class="menu-icon fa fa-road zmdi-hc-lg"></i>
            <span class="menu-text">Tour Drivers</span>
          </a>
        </li>

        <li class="has-submenu">
          <a href="{{asset('/tourguides')}}">
            <i class="menu-icon fa fa-male zmdi-hc-lg"></i>
            <span class="menu-text">Tour Guides</span>
          </a>
          <ul class="submenu">
          </ul>
        </li>

        <li class="menu-separator"><hr></li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon fa fa-car zmdi-hc-lg"></i>
            <span class="menu-text">Vehicle</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
					<ul class="submenu">
						<li>
							<a href="{{asset('/vehicle')}}">
		            <span class="menu-text">List Vehicle</span>
		          </a>
						</li>
						<li>
							<a href="{{asset('/addvehicle')}}">
		            <span class="menu-text">Add New</span>
		          </a>
						</li>
          </ul>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon fa fa-map-pin zmdi-hc-lg"></i>
            <span class="menu-text">Pick up Point</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
					<ul class="submenu">
						<li>
							<a href="{{asset('/pickup')}}">
		            <span class="menu-text">List Pick up Point</span>
		          </a>
						</li>
						<li>
							<a href="{{asset('/addpickup')}}">
		            <span class="menu-text">Add New</span>
		          </a>
						</li>
          </ul>
        </li>

		<li class="menu-separator"><hr></li>

		<li>
          <a href="{{asset('/logout')}}">
            <i class="menu-icon fa fa-sign-out"></i>
            <span class="menu-text">Log Out</span>
          </a>
        </li>

      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

@yield('content')

  <!-- APP FOOTER -->
  <div class="wrap p-t-0">
    <footer class="app-footer">
      <div class="clearfix">
        <ul class="footer-menu pull-right">
        </ul>
        <div class="copyright pull-left">Copyright : PT. Mandu Wisata Indonesia (2017)</div>
      </div>
    </footer>
  </div>
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

	<!-- build:js ../assets/js/core.min.js -->
	<script src="../new/libs/bower/jquery/dist/jquery.js"></script>
	<script src="../new/libs/bower/jquery-ui/jquery-ui.min.js"></script>
	<script src="../new/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js"></script>
	<script src="../new/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js"></script>
	<script src="../new/libs/bower/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="../new/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
	<script src="../new/libs/bower/PACE/pace.min.js"></script>
	<!-- endbuild -->

	<!-- build:js ../assets/js/app.min.js -->
	<script src="../new/assets/js/library.js"></script>
	<script src="../new/assets/js/plugins.js"></script>
	<script src="../new/assets/js/app.js"></script>
	<!-- endbuild -->
	<script src="../new/libs/bower/moment/moment.js"></script>
	<script src="../new/libs/bower/fullcalendar/dist/fullcalendar.min.js"></script>
	<script src="../new/assets/js/fullcalendar.js"></script>
</body>
</html>
