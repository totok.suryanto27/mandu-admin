<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mandu Administrator</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	<link rel="shortcut icon" sizes="196x196" href="../new/assets/images/mandu-logo-196x196.png">

	<link rel="stylesheet" href="../new/libs/bower/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../new/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="../new/libs/bower/animate.css/animate.min.css">
	<link rel="stylesheet" href="../new/assets/css/bootstrap.css">
	<link rel="stylesheet" href="../new/assets/css/core.css">
	<link rel="stylesheet" href="../new/assets/css/misc-pages.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">

	<style media="screen">
		body {
			background-image: url("../new/landinglogin.jpg");
			background-size: cover;
			background-repeat: no-repeat;
			background-position: center center;
		}
	</style>

</head>
<body class="simple-page">
	<div class="simple-page-wrap">
		<div class="simple-page-logo animated swing">
			<a href="index.html">
				<span></span>
				<span>Mandu</span>
			</a>
		</div><!-- logo -->
		<div class="simple-page-form animated flipInY" id="login-form">
	<h4 class="form-title m-b-xl text-center">Welcome Here! You May Login Below</h4>
	<form action="/login" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<input id="log-in-username" type="text" name="username" class="form-control" placeholder="Input Your Username">
		</div>

		<div class="form-group">
			<input id="log-in-password" type="password" name="password" class="form-control" placeholder="Input Your Password">
		</div>

		<div class="form-group m-b-xl">
			<div class="checkbox checkbox-primary">
				<input type="checkbox" id="keep_me_logged_in"/>
				<label for="keep_me_logged_in">Keep me signed in</label>
			</div>
		</div>
		<input type="submit" class="btn btn-deepOrange" value="LOG IN">
	</form>
</div><!-- #login-form -->

<div class="simple-page-footer">
	<p>
		<small></small>
	</p>
</div><!-- .simple-page-footer -->


	</div><!-- .simple-page-wrap -->
</body>
</html>
