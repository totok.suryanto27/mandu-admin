@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">CREATE NEW ATTRACTION</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
                        {!!
                            Form::open([
                                'action' => [
                                    'HomeController@storeAttraction'
                                ],
                                'method' => 'POST',
                                'class' => 'form-horizontal form-label-left input_mask',
                                'id' => 'update-on-the-fly-form',
                                'files' => 'true',
                            ])
                        !!}
							<div class="col-sm-3">
								<label for="exampleInputEmail1">Attraction Name</label>
								<input type="text" class="form-control"
                                       name="attraction-name"
                                       id=""
                                       value="{{ isset($data->att_name) ? $data->att_name : '' }}"
                                       placeholder="Attraction Name">
							</div>
							<div class="col-sm-3">
								<label for="exampleInputPassword1">City</label>
									<select id="city" name="city"
                                            class="form-control"
                                            value= {{ $data->city_id }}
                                            data-plugin="select2">
                                        <option>Select Your City</option>

                                        @foreach($city as $key => $item)
                                            @if ($item->parent_id != '1' && $item->parent_id != '0')
                                                @if ($data->city_id == $item->id)
                                                <option selected value="{{ $item->id }}">{{ $item->place_name }}</option>

                                                @endif
                                                <option value="{{ $item->id }}">{{ $item->place_name }}</option>

                                            @endif
                                        @endforeach
									</select>
							</div>
							<div class="col-sm-3">
								<label for="exampleInputEmail1">Opening Hours</label>
								<div class="input-group bootstrap-timepicker timepicker col-sm-9">
									<input id="timepicker2" name="opening-hours"
                                           type="text"
                                           value= {{ $data->openinghours }}
                                           class="form-control input-small" data-plugin="timepicker" data-options="{ showInputs: false, showMeridian: false }">
									<span class="input-group-addon bg-info"><i class="glyphicon glyphicon-time"></i></span>
								</div>
							</div>
							<div class="col-sm-3">
								<label for="exampleInputPassword1">Closing Hours</label>
								<div class="input-group bootstrap-timepicker timepicker col-sm-9">
									<input id="timepicker2" name="closing-hours"
                                           type="text"
                                           value= {{ $data->closinghours }}
                                           class="form-control input-small" data-plugin="timepicker" data-options="{ showInputs: false, showMeridian: false }">
									<span class="input-group-addon bg-info"><i class="glyphicon glyphicon-time"></i></span>
								</div>
							</div>
							<div class="col-sm-12">
								<p>Theme</p>
                                @foreach($type as $key => $item)
								<div class="checkbox checkbox-inline">
                                        <input id="theme" name="theme[]" type="checkbox" value="{{ $item->id }}">
                                        <label for="theme">{{ $item->attraction_type_name }}</label>
								</div>
                                @endforeach
							</div>
							<div class="col-sm-6">
								<div class="col-sm-6">
									<label for="">Latitude</label>
									<input name="lat"
                                           type="text"
                                           value={{ $data->lat }}
                                           class="form-control" id="lat" placeholder="Latitude">
								</div>
								<div class="col-sm-6">
									<label for="">Longitude</label>
									<input name="lng"
                                           type="text"
                                           value={{ $data->lot }} 
                                           class="form-control" id="lng" placeholder="Longitude">
								</div>
								<div class="widget">
									<hr class="widget-separator">
									<div class="widget-body">
										<input type="text"
											class="form-control"
											name="searchmap"
											id="searchmap">
										<div id="map-canvas" style="width: 100%;height: 400px;"></div>
										{!! Mapper::render() !!}
									</div><!-- .widget-body -->
								</div><!-- .widget -->
							</div>
							<div class="col-sm-6">
								<div class="">
									<label for="exampleInputPassword1">Difficulties Level</label>
										<select id="level" name="level" class="form-control" data-plugin="select2">
                                            <option>Select Level</option>

                                            @foreach($level as $key => $item)
                                                @if ($data->diff_level == $item->id)
                                                    <option selected value="{{ $item->id }}">{{ $item->diff_level }}</option>

                                                @endif
                                                    <option value="{{ $item->id }}">{{ $item->diff_level }}</option>
                                            @endforeach
										</select>
								</div>
								<div class="">
									<label for="exampleInputPassword1">Capacity</label>
										<select id="capacity" name="capacity" class="form-control" data-plugin="select2">
                                            <option>Capacity</option>

                                            @foreach($capacity as $key => $item)
                                                @if ($data->capacity == $item->id)
                                                    <option selected value="{{ $item->id }}">{{ $item->capacityseat }}</option>

                                                @endif
                                                    <option value="{{ $item->id }}">{{ $item->capacityseat }}</option>
                                            @endforeach
										</select>
								</div>
								<div class="form-group">
									<div class="checkbox">
                                        <input id="parking" name="parking" type="checkbox" value="1">
                                        <label for="parking">Parking</label><br>
									</div>
									<div class="checkbox">
                                        <input id="ac" name="ac" type="checkbox" value="1">
                                        <label for="ac">AC</label><br>
									</div>
									<div class="checkbox">
                                        <input id="atm" name="atm" type="checkbox" value="1">
                                        <label for="atm">ATM</label><br>
									</div>
									<div class="checkbox">
                                        <input id="giftshop" name="giftshop" type="checkbox" value="1">
                                        <label for="giftshop">Giftshop</label><br>
									</div>
									<div class="checkbox">
                                        <input id="toilet" name="toilet" type="checkbox" value="1">
                                        <label for="toilet">Toilet</label><br>
									</div>
									<div class="checkbox">
                                        <input id="resto" name="resto" type="checkbox" value="1">
                                        <label for="resto">Restaurant</label><br>
									</div>
									<div class="checkbox">
                                        <input id="mushola" name="mushola" type="checkbox" value="1">
                                        <label for="mushola">Mushola</label><br>
									</div>
									<div class="checkbox">
                                        <input id="wifi" name="wifi" type="checkbox" value="1">
                                        <label for="wifi">Wifi</label>
									</div>
								</div>
							</div>
						<div class="widget">
							<header class="widget-header">
							</header><!-- .widget-header -->
							<hr class="widget-separator">
							<div class="widget-body">
								{{-- <form action="../api/dropzone" class="dropzone" data-plugin="dropzone" data-options="{ url: '../api/dropzone'}"> --}}
									{{-- <div class="dz-message">
										<h3 class="m-h-lg">Drop files here or click to upload.</h3>
										<p class="m-b-lg text-muted">(This is just a demo dropzone. Selected files are not actually uploaded.)</p>
									</div> --}}
                                <input type="file" name="image">
								{{-- </form> --}}
							</div><!-- .widget-body -->
						</div><!-- .widget -->
						<button type="submit" class="btn btn-primary btn-md">Create</button>
                    {!! Form::close() !!}
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@include('new.scriptmap')

@endsection
