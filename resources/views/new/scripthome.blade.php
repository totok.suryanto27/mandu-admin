<script>
$(document).ready(function(){

    var locations = <?php print_r(json_encode($latlon)) ?>;

    var mymap = new GMaps({
      el: '#map-canvas',
      lat: -1.860938,
      lng: 117.095720,
      zoom:5
    });

    $.each( locations, function( index, value ){
	    mymap.addMarker({
	      lat: value.lat,
	      lng: value.lot,
	      title: value.att_name,
        click: function(e) {
	        alert(value.att_name);
	      }
	    });
   });

});

</script>
