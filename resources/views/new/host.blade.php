@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">{{ $dataCount }} Hosts</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Homestay Name</th>
										<th>Email</th>
										<th>Latitude</th>
										<th>Longitude</th>
										<th>Price</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($data as $host)

									<tr>
										<td>{{ $host->homestayname }}</td>
										<td>{{ $host->member_email }}</td>
										<td>{{ $host->lat }}</td>
										<td>{{ $host->lon }}</td>
                                        <td>{{ $host->price }}</td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@endsection
