@extends('new.mastertable')

@section('content')
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">ADD PICK UP POINT</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
            {!!
                Form::open([
                    'action' => [
                        'HomeController@storePickup'
                    ],
                    'method' => 'POST',
                    'files' => 'true',
                ])
            !!}
              <div class="form-group col-sm-12">

							<div class="form-group col-sm-3">
								<label for="exampleInputPassword1">Country</label>
									<select id="country" name="country" class="form-control" data-plugin="select2">
										<option value="Indonesia">INDONESIA</option>
									</select>
							</div>
							<div class="form-group col-sm-3">
								<label for="exampleInputPassword1">Province Name</label>
									<select id="province" name="province" class="form-control" data-plugin="select2">
                    <option>Select Province</option>

                    @foreach($city as $key => $item)
                        @if ($item->parent_id == '1')
                            <option value="{{ $item->place_name }}">{{ $item->place_name }}</option>

                        @endif
                    @endforeach
									</select>
							</div>
							<div class="form-group col-sm-3">
								<label for="exampleInputPassword1">City</label>
									<select id="city" name="city" class="form-control" data-plugin="select2">
                    <option>Select City</option>

                    @foreach($city as $key => $item)
                        @if ($item->parent_id != '1' && $item->parent_id != '0')
                            <option value="{{ $item->id }}">{{ $item->place_name }}</option>

                        @endif
                    @endforeach
									</select>
							</div>
							<div class="form-group col-sm-3">
								<label for="exampleInputPassword1">Pick up Type</label>
									<select id="type" name="type" class="form-control" data-plugin="select2">
                    <option>Select Type</option>

                    @foreach($pickType as $key => $item)
                            <option value="{{ $item->pickup_type_name }}">{{ $item->pickup_type_name }}</option>

                    @endforeach
									</select>
							</div>
							<div class="form-group col-sm-3">
								<label for="exampleInputEmail1">Place Name</label>
								<input type="text" class="form-control" id="place-name" name="place-name" placeholder="Place Name">
							</div>

            </div>
							<div class="form-group col-sm-12">
								<div class="form-group col-sm-6">
									<label for="exampleInputEmail1">Latitude</label>
									<input type="text" class="form-control" id="lat" name="lat" placeholder="Latitude">
								</div>
								<div class="form-group col-sm-6">
									<label for="exampleInputEmail1">Longitude</label>
									<input type="text" class="form-control" id="lng" name="lon" placeholder="Longitude">
								</div>
								<div class="widget">
									<hr class="widget-separator">
									<div class="widget-body">
                    <input type="text"
											class="form-control"
											name="searchmap"
											id="searchmap">
										<div id="map-canvas" style="width: 100%;height: 400px;"></div>
										{!! Mapper::render() !!}
									</div><!-- .widget-body -->
								</div><!-- .widget -->
							</div>
						<button type="submit" class="btn btn-primary btn-md">Create</button>
            {!! Form::close() !!}
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->

@include('new.scriptmap')

@endsection
