@extends('layouts.layout')

@section('content')
    <div id="page-wrapper">
        <div class="row">

            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-13 text-right">
                                <center>
                                    <div>Tour Bookings</div>
                                    <div>
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div>400</div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-13">
                                <center>
                                    <div>Homestay Bookings</div>
                                    <div>
                                        <i class="fa fa-home fa-5x"></i>
                                    </div>
                                    <div>300</div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-13">
                                <center>
                                    <div>Total Transactions</div>
                                    <div>
                                        <i class="fa fa-dollar fa-5x"></i>
                                    </div>
                                    <div>IDR, 10.000 K</div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-13">
                                <center>
                                    <div>Canceled Transactions</div>
                                    <div>
                                        <i class="fa fa-times fa-5x"></i>
                                    </div>
                                    <div>150</div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 align="center">Demographic Data</h3>
                            <div class="pull-right">
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div><img src="../img/demog.png"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <center>
                                                <a href="#" style="color:#FFFFFF"><img src="../assets/attraction-icon.png"></a>
                                            </center>
                                        </div>
                                        <div class="col-xs-4">
                                            <center>
                                                <a href="#" style="color:#FFFFFF"><img src="../assets/host-icon.png"></a>
                                            </center>
                                        </div>
                                        <div class="col-xs-4">
                                            <center>
                                                <a href="#" style="color:#FFFFFF"><img src="../assets/tour-guides-icon.png"></a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="" width="35" height="35">
                                            </td>

                                            <td width="100%">
                                                <span class="pull-right" style="color:#CCCCCC"><i class="fa fa-comment fa-2x"></i></span>
                                                Tangkuban Perahu
                                                <p class="text-muted small">200 Bookings
                                                </p>

                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#" class="list-group-item">
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="" width="35" height="35">
                                            </td>

                                            <td width="100%">
                                                <span class="pull-right" style="color:#CCCCCC"><i class="fa fa-comment fa-2x"></i></span>
                                                Brad Pitt
                                                <p class="text-muted small">Cheif Authority
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#" class="list-group-item">
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="" width="35" height="35">
                                            </td>

                                            <td width="100%">
                                                <span class="pull-right" style="color:#CCCCCC"><i class="fa fa-comment fa-2x"></i></span>
                                                Leonardo Dicaprio
                                                <p class="text-muted small">Cheif Authority
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#" class="list-group-item">
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="" width="35" height="35">
                                            </td>

                                            <td width="100%">
                                                <span class="pull-right" style="color:#CCCCCC"><i class="fa fa-comment fa-2x"></i></span>
                                                Jesse Eisenberg
                                                <p class="text-muted small">Cheif Authority
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#" class="list-group-item">
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="" width="35" height="35">
                                            </td>

                                            <td width="100%">
                                                <span class="pull-right" style="color:#CCCCCC"><i class="fa fa-comment fa-2x"></i></span>
                                                Kristen Stewart
                                                <p class="text-muted small">Cheif Authority
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#" class="list-group-item">
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="" width="35" height="35">
                                            </td>

                                            <td width="100%">
                                                <span class="pull-right" style="color:#CCCCCC"><i class="fa fa-comment fa-2x"></i></span>
                                                Ben Affleck
                                                <p class="text-muted small">Cheif Authority
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>

                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        <footer>
            <div>
                <p>Copyright : PT. Mandu Wisata Indonesia (2017)</p>
            </div>
        </footer>
    </div>

@endsection
