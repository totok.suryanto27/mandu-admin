<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('new.login');
});

// Route::get('/home', function () {
//     return view('new.home');
// });

// Route::get('/attraction', function () {
//     return view('new.attraction');
// });

// Route::get('/users', function () {
//     return view('new.users');
// });

// Route::get('/host', function () {
//     return view('new.host');
// });

// Route::get('/tourdriver', function () {
//     return view('new.tour_drivers');
// });
//
// Route::get('/tourguides', function () {
//     return view('new.tour_guides');
// });

// Route::get('/vehicle', function () {
//     return view('new.vehicle');
// });

// Route::get('/pickup', function () {
//     return view('new.pickup_point');
// });

// Route::get('/new_att', function () {
//     return view('new.new_attraction');
// });



Route::post('/login', 'LoginController@login');
Route::get('/home', 'HomeController@index');
Route::get('/attraction', 'HomeController@attraction');
Route::get('/attraction/{id}', 'HomeController@attractionReview');
Route::get('/edit_att/{id}', 'HomeController@editAtt');
Route::get('/delete_att/{id}', 'HomeController@deleteAtt');
Route::get('/new_att', 'HomeController@newAttraction');
Route::get('/users', 'HomeController@member');
Route::get('/tourdriver', 'HomeController@tourDriver');
Route::get('/tourguides', 'HomeController@tourGuides');
Route::get('/host', 'HomeController@homestay');
Route::get('/vehicle', 'HomeController@vehicle');
Route::get('/addvehicle', 'HomeController@addVehicle');
Route::get('/pickup', 'HomeController@pickup');
Route::get('/addpickup', 'HomeController@addPickup');
Route::get('/logout', 'LoginController@logout');
Route::post('/storeAttraction', 'HomeController@storeAttraction');
Route::post('/storePickup', 'HomeController@storePickup');
Route::post('/storeVehicle', 'HomeController@storeVehicle');
