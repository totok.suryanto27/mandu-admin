<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UserAdmin;
use App\Models\Attraction;
use App\Models\Difficulties;
use App\Models\Capacity;
use App\Models\City;
use App\Models\AttractionType;
use App\Models\MemberAttractionThemes;
use App\Models\MemberImages;
use App\Models\Member;
use App\Models\HomeStay;
use App\Models\TransportationType;
use App\Models\PickupPoint;
use App\Models\PickupType;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        // $user = UserAdmin::where('id', $id)
        //         ->first();

        $province = City::where('parent_id', '1')
                ->orderBy('id', 'asc')
                ->get();

        foreach ($province as $p) {
            $countData[] = Attraction::whereHas('city', function ($query) use ($p) {
                $query->where('parent_id', $p->id);
            })->count();

        }

        $userCount = Member::orderBy('id', 'asc')->get();

        $driverCount = Member::where('role', 3)->get();

        $guideCount = Member::where('role', 1)->get();

        $contributeCount = Member::where('role', 4)->get();

        $homestayCount = HomeStay::orderBy('id', 'asc')->get();

        $latlon = Attraction::all();

        return view('new.home', [
            // 'name' => $user->name,
            'latlon' => $latlon,
            'countData' => $countData,
            'province' => $province,
            'userCount' => count($userCount),
            'driverCount' => count($driverCount),
            'guideCount' => count($guideCount),
            'homestayCount' => count($homestayCount),
            'contributeCount' => count($contributeCount),
        ]);
    }

    public function attraction()
    {
        $data = Attraction::orderBy('id', 'dsc')->get();

        $data2 = Attraction::orderBy('id', 'dsc')->get();

        foreach ($data2 as $value) {
            $images[] = MemberImages::where('parent_id', $value->id)->first();

        }
        // dd($images);
        $path = "http://images.mandu.co.id";
        return view('new.attraction', [
            'data' => $data,
            'image' => $images,
            'path' => $path,
        ]);
    }

    public function attractionReview(Request $request, $id)
    {
        $data = Attraction::where('id', $id)
                ->first();

        $images = MemberImages::where('parent_id', $id)->get();

        $theme = MemberAttractionThemes::where('attr_id', $id)->get();
        foreach ($theme as $value) {
            $themes[] = AttractionType::where('id', $value->attr_type_id)->first();
        }

        $level = Difficulties::where('id', $data->diff_level)->first();

        $capacity = Capacity::where('id', $data->capacity)->first();

        $path = "http://imagesdev.mandu.co.id";

        return view('new.review_att', [
            'data' => $data,
            'image' => $images,
            'path' => $path,
            'level' => $level,
            'theme' => $themes,
            'capacity' => $capacity,
        ]);
    }

    public function newAttraction()
    {
        $city = City::orderBy('id', 'asc')->get();

        $type = AttractionType::all();

        $level = Difficulties::orderBy('id', 'asc')->get();

        $capacity = Capacity::orderBy('id', 'asc')->get();

        // Mapper::map(-6.914744, 107.609810);

        return view('new.new_attraction', [
            'city' => $city,
            'type' => $type,
            'level' => $level,
            'capacity' => $capacity,
        ]);
    }

    public function storeAttraction(Request $request)
    {
        $city = $request->input("city");
        $attname = $request->input("attraction-name");
        $lat = $request->input("lat");
        $lot = $request->input("lng");
        $capacity = $request->input("capacity");
        $openinghours = $request->input("opening-hours");
        $closinghours = $request->input("closing-hours");
        $parking = $request->input("parking");
        $ac = $request->input("ac");
        $mushola = $request->input("mushola");
        $atm = $request->input("atm");
        $giftshop = $request->input("giftshop");
        $resto = $request->input("resto");
        $toilet = $request->input("toilet");
        $wifi = $request->input("wifi");
        $att_type = $request->input("theme");
        $diff_level = $request->input("level");
// dd($request);
        $attraction = Attraction::create([
            "city_id" => $city,
            "member_email" => "mandu@mandu.co.id",
            "att_name" => $attname,
            "lat" => $lat,
            "lot" => $lot,
            "capacity" => $capacity,
            "openinghours" => $openinghours,
            "closinghours" => $closinghours,
            "parking" => $parking == "1" ? $parking : "0",
            "ac" => $ac == "1" ? $ac : "0",
            "mushola" => $mushola == "1" ? $mushola : "0",
            "atm" => $atm == "1" ? $atm : "0",
            "giftshop" => $giftshop == "1" ? $giftshop : "0",
            "resto" => $resto == "1" ? $resto : "0",
            "toilet" => $toilet == "1" ? $toilet : "0",
            "wifi" => $wifi == "1" ? $wifi : "0",
            "att_type" => count($att_type),
            "diff_level" => $diff_level,
            "post_user" => "adminmandu",
        ]);

        foreach ($att_type as $key => $value) {

            MemberAttractionThemes::create([
                "attr_id" => $attraction->id,
                "attr_type_id" => $value,
                "status" => "1",
                "post_user" => "adminmandu",
            ]);
        }

        $imgName = $request->image->getClientOriginalName();

        $request->image->move("/var/www/imagesdev/images"."/"."attractionimage/".date("Y")."/".date("m")."/".date("d"), $imgName);

        $imgcount = MemberImages::create([
            "image_type" => "ATT",
            "imagename" => $imgName,
            "filename" => $imgName,
            "member_email" => "mandu@mandu.co.id",
            "post_user" => "adminmandu",
            "status" => "1",
            "parent_id" => $attraction->id,
            "pathfile" => "attractionimage/".date("Y")."/".date("m")."/".date("d")."/",
        ]);

        Attraction::where('id', $attraction->id)
                    ->update([
                      'image_avail' => count($imgcount),
                      'post_date' => $attraction->created_at,
                    ]);

        return redirect('/attraction'.'/'.$attraction->id);
    }

    public function member(Request $request)
    {
        $data = Member::orderBy('id', 'asc')->get();

        return view('new.users', [
            'data' => $data,
            'dataCount' => count($data),
        ]);
    }

    public function tourDriver(Request $request)
    {
        $data = Member::orderBy('id', 'asc')->get();

        $count = Member::where('role', 3)->get();

        return view('new.tour_drivers', [
            'data' => $data,
            'dataCount' => count($count),
        ]);
    }

    public function tourGuides(Request $request)
    {
        $data = Member::orderBy('id', 'asc')->get();

        $count = Member::where('role', 1)->get();

        return view('new.tour_guides', [
            'data' => $data,
            'dataCount' => count($count),
        ]);
    }

    public function homestay(Request $request)
    {
        $data = HomeStay::orderBy('id', 'asc')->get();

        return view('new.host', [
            'data' => $data,
            'dataCount' => count($data),
        ]);
    }

    public function addVehicle(Request $request)
    {
        $data = TransportationType::orderBy('id', 'asc')->get();

        $ex = null;
        foreach ($data as $key => $value) {
          if ($ex != $value->brandname) {

            $brandname[] = $value->brandname;
          }
          $ex = $value->brandname;
        }

        for ($i=1900; $i < 2018 ; $i++) {
            $year[] = $i;
        }

        for ($k=1; $k < 61 ; $k++) {
            $capacity[] = $k;
        }

        return view('new.addvehicle', [
            'data' => $data,
            'year' => $year,
            'capacity' => $capacity,
            'brandname' => $brandname,
        ]);
    }

    public function storeVehicle(Request $request)
    {
        $vectype = $request->input("vectype");
        $brandname = $request->input("brandname");
        $brandtype = $request->input("brandtype");
        $year = $request->input("year");
        $capacity = $request->input("capacity");
        $mode = $request->input("mode");

        TransportationType::create([
            "moda" => $mode,
            "vectype" => $vectype,
            "brandname" => $brandname,
            "brandtype" => $brandtype,
            "yearprod" => $year,
            "capacity" => $capacity,
        ]);

        return redirect('/vehicle');
    }

    public function vehicle(Request $request)
    {
        $data = TransportationType::orderBy('id', 'asc')->get();

        return view('new.vehicle', [
            'data' => $data,
            'dataCount' => count($data),
        ]);
    }

    public function editAtt(Request $request, $id)
    {
        $data = Attraction::where('id', $id)
                ->first();

        $city = City::orderBy('id', 'asc')->get();

        $type = AttractionType::all();

        $level = Difficulties::orderBy('id', 'asc')->get();

        $capacity = Capacity::orderBy('id', 'asc')->get();

        // Mapper::map(-6.914744, 107.609810);

        return view('new.edit_att', [
            'data' => $data,
            'city' => $city,
            'type' => $type,
            'level' => $level,
            'capacity' => $capacity,
        ]);
    }

    public function deleteAtt(Request $request, $id)
    {
        $data = Attraction::where('id', $id)
                ->first();

        $data->delete();

        MemberAttractionThemes::where('attr_id', $id)
                ->delete();

        MemberImages::where('parent_id', $id)
                ->delete();

        return redirect('/attraction');
    }

    public function addPickup(Request $request)
    {
        $city = City::orderBy('id', 'asc')->get();

        $pickType = PickupType::orderBy('id', 'asc')->get();

        return view('new.addpickup_point', [
          'city' => $city,
          'pickType' => $pickType,
        ]);
    }

    public function storePickup(Request $request)
    {
        $cityId = $request->input("city");
        $cityName = City::where('id', $cityId)->first();
        $country = $request->input("country");
        $province = $request->input("province");
        $type = $request->input("type");
        $placeName = $request->input("place-name");
        $lat = $request->input("lat");
        $lon = $request->input("lon");

        PickupPoint::create([
          "id_city" => $cityId,
          "city_name" => $cityName->place_name,
          "place_name" => $placeName,
          "lat" => $lat,
          "lon" => $lon,
          "country_name" => $country,
          "province_name" => $province,
          "pickup_type" => $type,
        ]);

        return redirect('/pickup');

    }

    public function pickup(Request $request)
    {
        $data = PickupPoint::orderBy('id', 'asc')->get();

        return view('new.pickup_point', [
            'data' => $data,
            'dataCount' => count($data),
        ]);
    }

}
