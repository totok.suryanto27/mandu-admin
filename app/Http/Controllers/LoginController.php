<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Models\UserAdmin;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $checkuser = UserAdmin::where(['username' => $username, 'password' => $password])
                ->get();

        if (count($checkuser) > 0) {

            foreach ($checkuser as $user) {
                $redirectUrl = "/home";
                return redirect($redirectUrl);
            }
        } else {
            return redirect("/");
        }

    }

    public function logout()
    {
            Auth::logout();
            return redirect("/");
    }
}
