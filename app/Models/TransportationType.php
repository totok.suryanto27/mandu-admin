<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportationType extends Model
{
    protected $table = 'transportation_type';

    protected $fillable = [
        'moda',
        'vectype',
        'brandname',
        'brandtype',
        'yearprod',
        'capacity',
    ];
}
