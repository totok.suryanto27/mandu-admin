<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickupType extends Model
{
    protected $table = 'pickup_type';
}
