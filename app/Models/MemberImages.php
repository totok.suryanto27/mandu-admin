<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberImages extends Model
{
    protected $table = 'member_images';

    protected $fillable = [
        'image_type',
        'imagename',
        'filename',
        'member_email',
        'post_parent',
        'post_date',
        'post_user',
        'createddate',
        'status',
        'parent_id',
        'main_image',
        'pathfile',
    ];
}
