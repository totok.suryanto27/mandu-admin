<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickupPoint extends Model
{
    protected $table = 'pickup_point';

    protected $fillable = [
        'id_city',
        'city_name',
        'place_name',
        'lat',
        'lon',
        'post_parent',
        'post_date',
        'place_address',
        'country_name',
        'province_name',
        'pickup_type',
    ];
}
