<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeStay extends Model
{
    protected $table = 'member_homestays';
}
