<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttractionType extends Model
{
    protected $table = 'attraction_type';

    public function theme()
    {
      return $this->hasMany('App\Models\MemberAttractionThemes');
    }
}
