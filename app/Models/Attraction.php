<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attraction extends Model
{
    protected $table = 'member_attractions';

    protected $fillable = [
        'city_id',
        'member_email',
        'att_name',
        'lat',
        'lot',
        'capacity',
        'openinghours',
        'closinghours',
        'parking',
        'ac',
        'mushola',
        'atm',
        'giftshop',
        'resto',
        'toilet',
        'wifi',
        'att_type',
        'diff_level',
        'post_user',
        'image_avail',
    ];

    public function city()
    {
      return $this->belongsTo('App\Models\City');
    }

    public function level()
    {
      return $this->belongsTo('App\Models\Difficulties');
    }

    public function capacity_seat()
    {
      return $this->belongsTo('App\Models\Capacity');
    }
}
