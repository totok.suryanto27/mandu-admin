<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberAttractionThemes extends Model
{
    protected $table = 'member_attraction_themes';

    protected $fillable = [
        'attr_id',
        'attr_type_id',
        'status',
        'post_parent',
        'post_date',
        'post_user',
        'image_avail',
    ];

    public function type()
    {
      return $this->belongsTo('App\Models\AttractionType');
    }
}
