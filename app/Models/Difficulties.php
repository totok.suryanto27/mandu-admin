<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Difficulties extends Model
{
    protected $table = 'difficulties_level';


    public function attraction()
    {
        return $this->hasMany('App\Models\Attraction');
    }
}
