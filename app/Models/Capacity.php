<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Capacity extends Model
{
    protected $table = 'capacity_seats';


    public function attraction()
    {
        return $this->hasMany('App\Models\Attraction');
    }
}
